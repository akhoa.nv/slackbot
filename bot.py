from flask import Flask, request, make_response, Response, jsonify
import os
import json
import requests

from slackclient import SlackClient

TOKEN = 'xoxb-661131834755-672931172096-SI7irodmbSxxyyVviSZXrVnB'

app = Flask(__name__)

client = SlackClient(TOKEN)

@app.route("/buildconfig", methods=["POST"])
def build_config():
    return jsonify(response_type='in_channel', text='Build configuration <http://192.168.5.159:8080/job/CBCNavTest/configure|link>',)

@app.route("/slack", methods=["POST"])
def open_build_log():
    # Parse the request payload
    form_json = json.loads(request.form["payload"])

    #print(form_json['message']['attachments'][0]['fields'][0]['value'].split(' ')[5][1:])
    if (form_json['message']['attachments'][0]['fields'][0]['value'].split(' ')[5][1:].isnumeric()):
        open_dialog = client.api_call("dialog.open", trigger_id=form_json['trigger_id'],dialog={
                    "title": "Build log for " + form_json['message']['attachments'][0]['fields'][0]['value'].split(' ')[5],
                    "submit_label": "OK",
                    "callback_id": "open_build_log",
                    "elements": [
                        {
                            "label": "URL",
                            "type": "text",
                            "name": "build_log_url",
                            "value": "http://192.168.5.159:8080/job/CBCNavTest/" + form_json['message']['attachments'][0]['fields'][0]['value'].split(' ')[5][1:]  +"/consoleText"
                        }
                    ]
                }
            )

    return make_response("", 200)

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0', port=80)
